# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Support PostgreSQL as destination now. For this, you need the corresponding ODBC driver (cf. https://odbc.postgresql.org/)
- Support MariaDB as destination now. For this, you need the corresponding ODBC driver (cf. https://downloads.mariadb.org/connector-odbc/)

## [1.2.2] - 2020-02-22
### Added
- New parameter to CLI to indicate the number of rows per export file (for command export!)

### Changed
- Fix an issue where no split happened anymore during fetching data from database (put everything to memory)

## [1.2.1] - 2020-02-22
### Changed
- Fix an issue where column rename does not transfer the value correctly.

## [1.2.0] - 2020-01-01
### Added
- Validates whether alphanumeric data still fits into new table
- Auto shorten alphanumeric data if it does not fit into new table (argument/option)

### Changed
- Improved dry mode (see sep. option)

## [1.1.1] - 2019-12-12
### Added
- Added a CHANGELOG file in order to documentate all changes without need to read into all the commits.

### Changed
- Fix issue whereas it was not possible to use the replace function during data import or copy operation.

