# #!/usr/bin/env python3
# -*- coding: utf8 -*-
#
# Migrate script needs to do different things.
# @author Lukas Schreiner
#
import copy
import pyodbc
import sys
import datetime
import time
import os
import gzip
import glob
import logging
import base64
import argparse
import getpass
import xml
import traceback
import queue
import tqdm
from multiprocessing import JoinableQueue, Lock
from threading import Thread
from xml.dom.minidom import parseString

APP_NAME = 'MDSYNC'

class InvalidDataException(Exception):
	pass

class CopySettingsException(Exception):
	pass

class SyncReplaceMap(object):
	"""Replace values of a field through conditions."""

	def __init__(self, field: str, condition: str, newValue: str) -> None:
		self._field = field
		self._condition = condition
		self._newValue = newValue

	def match(self, col: str, colValue: str) -> bool:
		"""Check if given column and their value match to this defined rule"""
		return col.upper() == self._field and colValue == self._condition

	def apply(self, col: str) -> str:
		"""Returns the new value (should be called only in case of a match)"""
		return self._newValue

class SyncMergeMap(object):
	"""Merge several old fields in a new field through a defined pattern"""

	def __init__(self, field: str, fmt: str) -> None:
		self._field = field
		self._format = fmt
		self._reqFields = []

	def addField(self, fld: str) -> None:
		if fld not in self._reqFields:
			self._reqFields.append(fld)

	def match(self, colName: str) -> bool:
		return colName.upper() == self._field

	def apply(self, oldRow) -> str:
		attrs = {}
		for f in self._reqFields:
			attrs[f] = oldRow._values[f][1]

		return self._format.format(**attrs)

class SyncMapping(object):

	def __init__(self, table):
		self._table = table

		# we support:
		# 1. renames
		self._renames = {}
		# 2. field replacments
		self._replace = []
		# 3. merging of fields
		self._merge = []
		# 4. dropping of fields
		self._drops = []

	def addRename(self, old, new):
		if old.upper() not in self._renames.keys():
			self._renames[old.upper()] = new.upper()

	def addReplace(self, r):
		if r not in self._replace:
			self._replace.append(r)

	def addMerge(self, r):
		if r not in self._merge:
			self._merge.append(r)

	def addDrop(self, field):
		if field.upper() not in self._drops:
			self._drops.append(field.upper())

	def applyReplace(self, col, colValue):
		for rule in self._replace:
			if rule.match(col, colValue):
				return rule.apply(col)

		return colValue

	def applyMerge(self, newField, oldRow):
		for rule in self._merge:
			if rule.match(newField):
				return rule.apply(oldRow)

		return None

class SyncFilter(object):

	def __init__(self, table):
		self._table = table
		self._rules = []

	def addRule(self, rule):
		self._rules.append(rule)

	def getRules(self):
		return ' AND '.join(self._rules)

class SyncConfig(object):

	def __init__(self, cfgFile):
		self._source = cfgFile
		self._srcDsn = None
		self._srcEngine = None
		self._srcUser = None
		self._srcPwd = None
		self._srcKku = None
		self._srcVersion = None
		self._srcRelease = None
		self._dstDsn = None
		self._dstEngine = None
		self._dstUser = None
		self._dstPwd = None
		self._dstKku = None
		self._dstVersion = None
		self._dstRelease = None
		self._dstTransaction = None
		self._mapping = {}
		self._rules = {}
		self._tables = {}
		self._truncations = {}
		self._tableRename = {}

	def compileSourceDsn(self):
		if self._srcDsn is not None:
			if not self._srcPwd:
				pwd = getpass.getpass('Password for source {:s}: '.format(self._srcUser))
			else:
				try:
					# Python 3
					pwd = base64.b64decode(self._srcPwd).decode('utf-8')
				except NameError:
					# Python 2
					pwd = base64.b64decode(self._srcPwd)
			try:
				self._srcDsn = self._srcDsn.replace('{user}', self._srcUser).replace('{pwd}', pwd)\
					.replace('{app}', APP_NAME)
			except:
				pass

	def compileDestinationDsn(self):
		if self._dstDsn is not None:
			if not self._srcPwd:
				pwd = getpass.getpass('Password for destination {:s}: '.format(self._dstUser))
			else:
				try:
					# Python 3
					pwd = base64.b64decode(self._dstPwd).decode('utf-8')
				except NameError:
					# Python 2
					pwd = base64.b64decode(self._dstPwd)
			try:
				self._dstDsn = self._dstDsn.replace('{user}', self._dstUser).replace('{pwd}', pwd)\
					.replace('{app}', APP_NAME)
			except:
				pass

	def compileDsn(self):
		self.compileSourceDsn()
		self.compileDestinationDsn()

	def getFullSourceLib(self, lib):
		return self._srcKku + lib + self._srcVersion

	def getFullDestinationLib(self, lib):
		return self._dstKku + lib + self._dstVersion

	def isPartOfConfig(self, fileName):
		"""
		Check if this file is intended for import!
		"""

		# first extract the file name without extension.
		(baseFileName, fileExt) = os.path.splitext(os.path.basename(fileName))

		# the file has following scheme: <prefix>_<scheme>_<table>_<rowNum>
		# We're not interested in prefix, scheme (can be different) and rowNum.
		# rowNum may also contain .xml at the end (normally we export the data into .xml.gz)
		(prefix, scheme, tableName, rowNum) = baseFileName.split('_', 4)
		tableName = tableName.upper()

		for tableLists in self._tables.values():
			for table in tableLists:
				if tableName == table.upper():
					return True

		return False

	def map(self, table, column):
		try:
			return self._mapping[table.upper()]._renames[column.upper()]
		except:
			return column

	def drop(self, table, column):
		try:
			return column.upper() in self._mapping[table.upper()]._drops
		except:
			return False

	def getColumnValue(self, table, col, colValue):
		newValue = colValue
		# check if there is any configuration
		if table in self._mapping:
			newValue = self._mapping[table].applyReplace(col, colValue)

		return newValue

	def merge(self, table, col, oldRow):
		if table in self._mapping:
			return self._mapping[table].applyMerge(col, oldRow)

		return None

	def parseConfig(self, parent=None, child=None):
		with open(self._source, 'rb') as f:
			try:
				data = parseString(f.read())
			except xml.parsers.expat.ExpatError as e:
				sys.stderr.write('Could not parse config file ({:s})!\n'.format(str(e)))
				sys.exit(1)
			root = data.firstChild #EPSYNC
			self.parseNode(parent=None, node=root)

	def parseNode(self, parent=None, node=None):
		for child in node.childNodes:
			g = child.nodeName
			if parent is None:
				funcName = 'parse' + g.capitalize()
			else:
				funcName = 'parse' + parent.capitalize() + g.capitalize()
			if hasattr(self, funcName):
				func = getattr(self, funcName)
				func(g, child)
			else:
				self.parseNode(g, child)

	def getNodeValue(self, node):
		txt = ''
		for txtNode in node.childNodes:
			txt += txtNode.wholeText

		return txt

	def _parseConnection(self, parent, conNode, origin):
		nodeName = conNode.nodeName
		for child in conNode.childNodes:
			g = child.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				attrName = '_' + origin + g.capitalize()
				if hasattr(self, attrName):
					setattr(self, attrName, self.getNodeValue(child))
				else:
					sys.stderr.write('Don\'t understand tag {:s}\n'.format(g))
					sys.exit(1)

	def parseConnectionSource(self, parent, conNode):
		self._parseConnection(parent, conNode, 'src')

	def parseConnectionDestination(self, parent, conNode):
		self._parseConnection(parent, conNode, 'dst')

	def parseTables(self, parent, mapNode):
		for schema in mapNode.childNodes:
			g = schema.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				self._tables[g.strip().upper()] = []
				self._parseTablesInSchema(g.strip().upper(), schema)

	def _parseTablesInSchema(self, schema, mapNode):
		for table in mapNode.childNodes:
			g = table.nodeName
			if g in ['#text', '#comment']:
				pass
			elif g.strip().upper() not in self._tables[schema]:
				self._tables[schema].append(g.strip().upper())
				# Do we have to truncate the table prior import of data?
				if table.hasAttribute('truncate'):
					truncateOption = table.getAttribute('truncate').lower().strip()
					if truncateOption in ['yes', 'true']:
						self._truncations[g.strip().upper()] = True
				# Is the destination a different table scheme/name?
				if table.hasAttribute('destination'):
					self._tableRename[g.strip().upper()] = table.getAttribute('destination').strip().upper()
				if table.hasChildNodes():
					self._parseTableRules(schema, table)
			else:
				sys.stderr.write('Don\'t understand tag {:s}\n'.format(g))
				sys.exit(1)

	def _parseTableRules(self, schema, table):
		for rules in table.childNodes:
			g = rules.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				attrName = '_parseTableRule' + g.capitalize()
				if hasattr(self, attrName):
					self._rules[table.nodeName] = SyncFilter(table.nodeName)
					getattr(self, attrName)(schema, table, self._rules[table.nodeName], rules)
				else:
					sys.stderr.write('Don\'t understand tag {:s}\n'.format(g))
					sys.exit(1)

	def _parseTableRuleCondition(self, schema, table, smap, rule):
		# for now, there is exactly one way: to just write the formular!
		rawRule = self.getNodeValue(rule)
		smap.addRule(rawRule)

	def parseMapping(self, parent, mapNode):
		for table in mapNode.childNodes:
			g = table.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				self._mapping[g] = SyncMapping(g)
				self._parseTableMapping(self._mapping[g], table)

	def _parseTableMapping(self, smap, xmlNode):
		for child in xmlNode.childNodes:
			g = child.nodeName
			attrName = '_parseField' + g.capitalize()
			if g in ['#text', '#comment']:
				pass
			elif hasattr(self, attrName):
				getattr(self, attrName)(smap, child)
			else:
				sys.stderr.write('Could not parse the mapping area (unknown tag {:s})!\n'.format(g))
				sys.exit(1)

	def _parseFieldRename(self, smap, xmlNode):
		for child in xmlNode.childNodes:
			g = child.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				fieldName = g
				# get new name!
				newName = child.firstChild.wholeText.strip()
				smap.addRename(fieldName, newName)

	def _parseFieldMerge(self, smap, xmlNode):
		for child in xmlNode.childNodes:
			g = child.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				fieldName = g
				newValue = child.firstChild.wholeText.strip()
				# but what the hack is the condition (None is allowed too!)?
				fmt = None
				if not child.hasAttribute('format'):
					sys.stderr.write('Missing format attr for field {:s}!\n'.format(g))
					sys.exit(1)
				else:
					fmt = child.getAttribute('format')
				r = SyncMergeMap(fieldName, fmt)
				# now add all the fields
				for oldFields in child.childNodes:
					if oldFields.nodeName == '#text':
						pass
					else:
						r.addField(oldFields.nodeName.upper().strip())
				smap.addMerge(r)

	def _parseFieldReplace(self, smap, xmlNode):
		for child in xmlNode.childNodes:
			g = child.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				fieldName = g
				newValue = child.firstChild.wholeText.strip()
				# but what the hack is the condition (None is allowed too!)?
				condition = None
				if child.hasAttribute('when'):
					condition = child.getAttribute('when')
				r = SyncReplaceMap(fieldName, condition, newValue)
				smap.addReplace(r)

	def _parseFieldDrop(self, smap, xmlNode):
		for child in xmlNode.childNodes:
			g = child.nodeName
			if g in ['#text', '#comment']:
				pass
			else:
				smap.addDrop(g.strip().upper())

class Column(object):

	DATA_TYPE_ALPHA = 1
	DATA_TYPE_NUMERIC = 2
	DATA_TYPE_GRAPHIC = 8

	XML_CHAR_MAP = {
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		'&': '&amp;',
		"'": '&apos;'
	}

	def __init__(self, colname, datatype, typename, colsize, buflen, decdigit, nullable):
		self._name = colname
		self._data = datatype
		self._type = typename
		self._size = colsize
		self._buflen = buflen
		self._decimal = decdigit if decdigit is not None else 0
		self._nullable = nullable

	@staticmethod
	def unifyDataType(dataType, engine):
		if dataType < 0:
			dataType = dataType*(-1)
		if engine == 'DB2':
			if dataType == 1:
				return Column.DATA_TYPE_ALPHA
			elif dataType == 2:
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 8:
				return Column.DATA_TYPE_GRAPHIC
		elif engine == 'PSQL':
			if dataType == 2: # numeric
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 4: # int4
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 5: # int2
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 8: # bpchar
				return Column.DATA_TYPE_GRAPHIC
		elif engine == 'MARIADB':
			if dataType == 3: # decimal
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 4: # int
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 5: # smallint
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 8: # char (nchar + char seems same)
				return Column.DATA_TYPE_GRAPHIC
		else:
			if dataType == 12:
				return Column.DATA_TYPE_ALPHA
			elif dataType == 2:
				return Column.DATA_TYPE_NUMERIC
			elif dataType == 9:
				return Column.DATA_TYPE_GRAPHIC

	def is_hidden(self):
		return True if self._name in ['ROWNUM', 'ROWNUMBER'] else False

	def isAlpha(self):
		return True if self._data == Column.DATA_TYPE_ALPHA else False

	def isNumeric(self):
		return True if self._data == Column.DATA_TYPE_NUMERIC else False

	def isInteger(self):
		return True if self.isNumeric() and self._decimal == 0 else False

	def isFloat(self):
		return True if self.isNumeric() and self._decimal > 0 else False

	def isGraphic(self):
		return True if self._data == Column.DATA_TYPE_GRAPHIC else False

	def pythonValue(self, value):
		retval = None

		if self.isAlpha() or self.isGraphic():
			if value:
				if type(value).__name__ == 'unicode':
					d = unicode(value)
					bytelist = ''
					for e in d:
						bytelist += '{:0>2s}'.format(hex(ord(e))[2:])

					retval = bytearray.fromhex(bytelist).decode('latin1').encode('utf-8').replace(b'\x00', b'\x20').decode('utf-8')
				else:
					retval = str(value).rstrip()
					# check if valid character
					retval = retval.encode('utf-8').replace(b'\x00', b'\x20').decode('utf-8')
			else:
				retval = str('')
		elif self.isInteger():
			if value:
				retval = int(value)
			else:
				retval = int(0)
		elif self.isFloat():
			if value:
				retval = float(value)
			else:
				retval = float(0.0)
		else:
			retval = value

		return retval

	def xmlValue(self, value):
		if not hasattr(value, 'replace'):
			strval = str(value)
		else:
			strval = value

		# avoid duplicate encoding... 
		strval = strval.replace('&', '&amp;')
		for origChar, replChar in Column.XML_CHAR_MAP.items():
			if origChar != '&':
				strval = strval.replace(origChar, replChar)
		return strval

	def toSqlValue(self, value):
		if self.isAlpha() or self.isGraphic():
			return '\'' + value.replace('\'', '\\\'') + '\''
		else:
			return str(value)

	def typeDesc(self):
		if self.isAlpha():
			return 'string'
		elif self.isGraphic():
			return 'graphic'
		elif self.isInteger():
			return 'integer'
		elif self.isFloat():
			return 'float'
		else:
			raise Exception('Cannot determine the data type of column {:s} (Type: {:s})'.format(self._name, str(self._data)))
	
	def validateData(self, colValue):
		"""
		Checks if the corresponding alphanumeric content fits into column of new table.
		"""
		# only supported for string!
		if (self.isAlpha() or self.isGraphic()) and len(colValue) > self._size:
			return False
		else:
			return True
	
	def shortenData(self, colValue):
		"""
		Shortens data to fit into the new column.
		"""
		if (self.isAlpha() or self.isGraphic()) and len(colValue) > self._size:
			return colValue[:self._size]
		else:
			return colValue

	def toMetaXml(self):
		try:
			xml  = '\t\t\t<column name="{:s}" typename="{:s}" data="{:d}" type="{:s}" size="{:d}" buflen="{:d}" decimal="{:d}" null="{:d}" />'.format(
				self._name, self.typeDesc(), self._data, self._type, self._size, self._buflen, self._decimal, self._nullable
			) + os.linesep
		except TypeError as e:
			print(self._name, self.typeDesc(), self._data, self._type, self._size, self._buflen, self._decimal, self._nullable)
			raise e
		return xml

	def toDataXml(self, coldata):
		xml  = '\t\t\t\t<column name="{:s}">'.format(self._name)
		xml += self.xmlValue(coldata)
		xml += '</column>' + os.linesep
		return xml

	@classmethod
	def parseXml(cls, xml):
		name = xml.getAttribute('name')
		#print('\t\t\tParsing column {:s}...'.format(name))
		data = int(xml.getAttribute('data'))
		typename = xml.getAttribute('type')
		size = int(xml.getAttribute('size'))
		buflen = int(xml.getAttribute('buflen'))
		decimal = int(xml.getAttribute('decimal'))
		nullable = int(xml.getAttribute('null'))
		self = cls(name, data, typename, size, buflen, decimal, nullable)

		return self

class Row(object):

	def __init__(self):
		self._values = {}

	def setData(self, colDef, colData):
		col = [colDef, colDef.pythonValue(colData)]
		self._values[colDef._name] = col

	def updateColumnDefinition(self, colDef):
		self._values[colDef._name] = [colDef, self._values[colDef._name][1]]

	def toXml(self, fd = None):
		xml  = '\t\t\t<row cols="{:d}">'.format(len(self._values.keys())) + os.linesep
		for colname, (colDef, colData) in self._values.items():
			xml += colDef.toDataXml(colData)
			if fd is not None:
				fd.write(xml.encode('utf-8'))
				xml = ''
		xml += '\t\t\t</row>' + os.linesep
		if fd is not None:
			fd.write(xml.encode('utf-8'))
			xml = ''

		return xml

	def toSql(self, colsSource, colsDestination, table, cfg):
		sql = '('
		firstCol = True
		for col in colsDestination:
			(colDef, val) = self._values[col]
			if not firstCol:
				sql += ', '
			# do we need to replace the content?
			val = cfg.getColumnValue(table, col, val)
			sql += colDef.toSqlValue(val)
			firstCol = False
		sql += ')'
		return sql

	def toSqlList(self, colsSource, colsDestination, table, cfg, dryMode=False, shorten=False):
		params = []
		sql = '('
		firstCol = True
		for col in colsDestination:
			(colDef, val) = self._values[col]
			if not firstCol:
				sql += ', '
			sql += '?'
			val = cfg.getColumnValue(table, col, val)
			if (dryMode or shorten) and not colDef.validateData(val):
				sys.stderr.write('\n\t\tIssue in col {:s}: value is too long{:s}: {:s}'.format(
					col, ' (auto shorten)' if shorten else '', val
				))
				if shorten:
					val = colDef.shortenData(val)
				else:
					raise InvalidDataException(
						'Error preparing data in table {table}, column {column}: data "{data}" too long (max: {newLength}, is: {currentLength})'.format(
							table=table, column=col, data=val, newLength=colDef._size, currentLength=len(val)
						)
					)

			params.append(val)
			firstCol = False
		sql += ')'
		return (sql, tuple(params))

	@classmethod
	def parseXml(cls, colDef, rowxml):
		#print('\t\tParsing row...')
		self = cls()
		# get each column
		for col in rowxml.childNodes:
			if col.nodeType == 1:
				colName = col.getAttribute('name')
				# try to fetch the column
				column = colDef[colName]
				colData = ''
				for colText in col.childNodes:
					if colText.nodeType == 3:
						colData += colText.wholeText
				self.setData(column, colData)

		return self

class Table(object):

	def __init__(self, lib, table):
		self._lib = lib
		self._table = table
		self._columns = []
		self._insertCols = []
		self._rows = []
		self._where = ''

	def libtab(self, engine, kku=None, version=None, newLib=None, newName=None):
		tableName = newName if newName else self._table
		part1 = kku if kku else self._lib[:3]
		part2 = newLib if newLib else self._lib[3:6]
		part3 = version if version else self._lib[6:]
		lib = part1 + part2 + part3
		if engine != 'DB2':
			lib = lib[:3] + 'DAT' + lib[6:]
		return '{:s}.{:s}'.format(lib, tableName)

	def renameTable(self, newLib, newName):
		"""
		Renames the table from A to B. 
		Remember: self._lib contains full qualified name. newLib contains only the 
		middle part (three chars).
		"""
		self._table = newName
		self._lib = self._lib[:3] + newLib + self._lib[6:]

	def updateDestination(self, kku, version):
		self._lib = kku + self._lib[3:6] + version

	def mergeConditions(self, conditions):
		if self._where:
			self._where += ' AND '
		else:
			self._where += 'WHERE '
		self._where += conditions.getRules()

	def applyRules(self, cfg, newTable):
		"""
		Figure out which columns exist on old table and on new table in order to
		match and import data to the new table structure.
		"""
		# compare columns
		colPrev = {}
		colNext = {}
		for col in self._columns:
			if not col.is_hidden():
				colPrev[col._name] = col

		for col in newTable._columns:
			if not col.is_hidden():
				colNext[col._name] = col

		colBoth = list(set(list(colPrev.keys()) + list(colNext.keys())))
		colRequired = []
		colFinal = []
		merge = []
		rename = []

		for cName in colBoth:
			if cName in colPrev.keys() and cName in colNext.keys():
				colRequired.append(cName)
				colFinal.append(cName)
			elif cName != cfg.map(self._table, cName) and cfg.map(self._table, cName) in colNext.keys():
				newName = cfg.map(self._table, cName)
				rename.append(cName)
				self._columns.append(colNext[newName])
				if cName not in colRequired:
					colRequired.append(cName)
				if newName not in colFinal:
					colFinal.append(newName)
			elif cName in colNext.keys():
				# merge?
				self._columns.append(colNext[cName])
				merge.append(cName)
				if cName not in colRequired:
					colRequired.append(cName)
				if cName not in colFinal:
					colFinal.append(cName)
			else:
				# old column is dropped....
				pass

		for row in self._rows:
			for fld in colRequired:
				if fld in merge:
					newValue = cfg.merge(self._table, fld, row)
					row.setData(colNext[fld], newValue)
				elif fld in rename:
					newName = cfg.map(self._table, fld)
					newValue = cfg.merge(self._table, newName, row)
					if not newValue:
						newValue = row._values[fld][1]
					row.setData(colNext[newName], newValue)
				else:
					row.updateColumnDefinition(colNext[fld])

		self._insertCols = colFinal

	def clear(self):
		del(self._rows)
		self._rows = []

	def fetchColumns(self, sock, srcEngine):
		cx = sock.cursor()

		# get list of columns
		lib = self._lib if srcEngine == 'DB2' else self._lib[:3] + 'DAT' + self._lib[6:]
		if srcEngine == 'PSQL':
			self._table = self._table.lower()
			lib = lib.lower()

		# check before, if the table really exist. Note: this does not work with DB2 driver!
		tabLookupSchema = lib if srcEngine != 'MARIADB' else None
		tabLookupCatalog = lib if srcEngine == 'MARIADB' else None
		if not cx.tables(table=self._table, schema=tabLookupSchema, catalog=tabLookupCatalog).fetchone() \
			and srcEngine != 'DB2':
			raise Exception('Table {:s} does not exist!'.format(self._table))
			
		cx.columns(table=self._table, schema=tabLookupSchema, catalog=tabLookupCatalog)
		# get columns.
		for rows in cx.fetchall():
			if srcEngine == 'DB2':
				(table_cat, table_schem, table_name, column_name, data_type, type_name, \
				column_size, buffer_length, decimal_digits, num_prec_radix, nullable, \
				remarks, column_def, sql_data_type, sql_datetime_sub, char_octet_length, \
				ordinal_position, is_nullable) = rows
			elif srcEngine == 'MSSQL':
				(table_cat, table_schem, table_name, column_name, data_type, type_name, \
				column_size, buffer_length, decimal_digits, num_prec_radix, nullable, \
				remarks, column_def, sql_data_type, sql_datetime_sub, char_octet_length, \
				ordinal_position, is_nullable, somevalue1, somevalue2, somevalue3, somevalue4, \
				somevalue5, somevalue6, somevalue7, somevalue8, somevalue9, somevalue10, somevalue11) = rows
			elif srcEngine == 'ORACLE':
				(table_cat, table_schem, table_name, column_name, data_type, type_name, \
				column_size, buffer_length, decimal_digits, num_prec_radix, nullable, \
				remarks, column_def, sql_data_type, sql_datetime_sub, char_octet_length, \
				ordinal_position, is_nullable) = rows
			elif srcEngine == 'PSQL':
				(table_cat, table_schem, table_name, column_name, data_type, type_name, \
				column_size, buffer_length, decimal_digits, num_prec_radix, nullable, \
				remarks, column_def, sql_data_type, sql_datetime_sub, char_octet_length, \
				ordinal_position, is_nullable, somevalue1, somevalue2, somevalue3, somevalue4, \
				somevalue5, somevalue6, somevalue7, somevalue8) = rows
				# PostgreSQL is case sensitive. Uh oh...
				column_name = column_name.upper()
			elif srcEngine == 'MARIADB':
				(table_cat, table_schem, table_name, column_name, data_type, type_name, \
				column_size, buffer_length, decimal_digits, num_prec_radix, nullable, \
				remarks, column_def, sql_data_type, sql_datetime_sub, char_octet_length, \
				ordinal_position, is_nullable) = rows
			else:
				raise Exception('Unknown database engine selected: {:s}'.format(srcEngine))

			# skip KEY$ and ROWNUMBER columns
			if not column_name.startswith('KEY$') and not column_name.startswith('ROWNUM') and not column_name.startswith('SST_'):
				udtp = Column.unifyDataType(data_type, srcEngine)
				if not udtp:
					raise Exception('Unknown column data type {:s} for column {:s}'.format(type_name, column_name))
				col = Column(column_name, udtp, type_name, column_size, buffer_length, decimal_digits, nullable)
				self._columns.append(col)

		cx.close()

		if not self._columns:
			raise Exception('Could not retrieve any column definition for table {:s}.{:s}'.format(lib, self._table))

	def getColumnByName(self, colname):
		for col in self._columns:
			if col._name == colname:
				return col

	def getNumberRows(self, sock, lib, where):
		sql = 'SELECT COUNT(*) FROM {:s} {:s}'.format(lib, where)
		cx = sock.cursor()
		cx.execute(sql)
		rowNumber = cx.fetchone()
		cx.close()

		return int(rowNumber[0])

	def fetchData(self, sock, cfg, dstSock=None, dryMode=False, shorten=False, resume=False, 
			rowsFetch=100, singleExportFile=True, noRowsPerFile=1000, saveHook=None):

		# log all information
		log = logging.getLogger('MD-Sync')

		# check some parameter!
		if rowsFetch <= 0:
			raise Exception('Number of rows to fetch must be min. 1')
		if saveHook and noRowsPerFile <= 0:
			raise Exception('In case the records should be saved, parameter noRowsPerFile can\t be empty.')

		tableName = self.libtab(cfg._srcEngine)
		# In case we must resume, we first need to determine the total amount 
		# of data in destination socket!
		numberSyncedRows = 0
		if resume:
			# does not work without a destination socket (which is optional)
			if not dstSock:
				raise CopySettingsException('Cannot resume without getting a dstSock!')
			# get total amount of lines for destination table.
			try:
				dstTableLib, dstTableName = cfg._tableRename[self._table.upper()].split('/')
			except KeyError:
				dstTableLib = dstTableName = None
			dstTableName = self.libtab(cfg._dstEngine, cfg._dstKku, 
								cfg._dstVersion, dstTableLib, dstTableName)
			numberSyncedRows = self.getNumberRows(dstSock, dstTableName, self._where)
			log.info('... resuming {:s} after {:d} rows.'.format(
				self._table, numberSyncedRows
			))
			# check the source file!
			numberSourceRows = self.getNumberRows(sock, tableName, self._where)
			# target already reached?
			if numberSyncedRows >= numberSourceRows:
				log.info(
					'[{:s}] Copy to {:s} with {:d} out of {:d} rows already done!'.format(
						tableName, dstTableName, numberSyncedRows, numberSourceRows
					)
				)
				return
			else:
				log.info(
					'[{:s}] Copy to {:s} with {:d} out of {:d} rows to be continued.'.format(
						tableName, dstTableName, numberSyncedRows, numberSourceRows
					)
				)

			# ensure, no truncation is done!
			cfg._truncations[self._table.upper()] = False

		# build SQL
		collist = self.colnames(cfg)
		sql = 'SELECT {:s} FROM {:s} {:s}'.format(', '.join(collist), tableName, self._where)
		cx = sock.cursor()
		cx.execute(sql)
		recordsRead = 0
		totalRows = 0
		totalReadRecords = 0
		dataFound = True
		# do we need to skip rows?
		# might be to have a state file later.
		if resume and numberSyncedRows > 0:
			cx.skip(numberSyncedRows)
			totalReadRecords = numberSyncedRows

		# FIXME: restructure and put copy / save and logging operation to 
		# separate function.
		while dataFound:
			dataFound = False
			for row in cx.fetchmany(rowsFetch):
				recordsRead += 1
				totalReadRecords += 1

				# parse row - ensure, enough memory is available.
				try:
					r = Row()
					for i in range(0, len(collist)):
						r.setData(self.getColumnByName(collist[i]), row[i])
					self._rows.append(r)
				except MemoryError as e:
					raise e
				dataFound = True
				if not singleExportFile and saveHook:
					if recordsRead >= noRowsPerFile:
						log.debug('[{:s}] ... fetched {:d}'.format(tableName, totalReadRecords))
						log.debug('[{:s}] reached limit of {:d} records. Saving now.'.format(tableName, noRowsPerFile))
						totalRows = saveHook(self, dstSock, singleExportFile, noRowsPerFile, totalRows, dryMode, shorten)
						recordsRead = 0

			log.debug('[{:s}] ... fetched {:d}'.format(tableName, totalReadRecords))
			if not singleExportFile and saveHook and recordsRead >= noRowsPerFile:
				log.info('[{:s}] reached limit of {:d} records. Saving now.'.format(tableName, noRowsPerFile))
				# if there are still data open and we have to call a hook, do it!
				if not singleExportFile and saveHook and self._rows:
					totalRows = saveHook(self, dstSock, singleExportFile, noRowsPerFile, totalRows, dryMode, shorten)
					recordsRead = 0

		# source cursor not required anymore.
		cx.close()
		if saveHook and recordsRead > 0:
			log.info('[{:s}] Final save of table.'.format(self._table, noRowsPerFile))
			# if there are still data open and we have to call a hook, do it!
			if not singleExportFile and saveHook and self._rows:
				totalRows = saveHook(self, dstSock, singleExportFile, noRowsPerFile, totalRows, dryMode, shorten)
				recordsRead = 0

		return totalRows

	def colnames(self, cfg, drop=False):
		if drop:
			return [ col._name for col in self._columns if not col.is_hidden() and not cfg.drop(self._table, col._name)]
		else:
			return [ col._name for col in self._columns if not col.is_hidden() ]
		
	def newColNames(self, cfg):
		return self._insertCols

	def toXml(self, fd = None):
		xml  = '\t<table library="{:s}" name="{:s}" rows="{:d}">'.format(
			self._lib, self._table, len(self._rows)
		) + os.linesep
		xml += '\t\t<columns>' + os.linesep
		if fd is not None:
			fd.write(xml.encode('utf-8'))
			xml = ''

		# prepare meta column information
		for col in self._columns:
			xml += col.toMetaXml()
		if fd is not None:
			fd.write(xml.encode('utf-8'))
			xml = ''
		xml += '\t\t</columns>' + os.linesep
		xml += '\t\t<rows>' + os.linesep
		if fd is not None:
			fd.write(xml.encode('utf-8'))
			xml = ''

		# write rows to file.
		for row in self._rows:
			xml += row.toXml(fd=fd)
		xml += '\t\t</rows>' + os.linesep
		xml += '\t</table>' + os.linesep
		if fd is not None:
			fd.write(xml.encode('utf-8'))
			xml = ''
		return xml

	def popXml(self, maxRows=None):
		rows = len(self._rows)
		if rows > 0:
			if maxRows is not None and rows > maxRows:
				rows = maxRows
			selRows = rows
			xml  = '\t<table library="{:s}" name="{:s}" rows="{:d}">'.format(
				self._lib, self._table, rows
			) + os.linesep
			xml += '\t\t<columns>' + os.linesep

			# prepare meta column information
			for col in self._columns:
				xml += col.toMetaXml()

			xml += '\t\t</columns>' + os.linesep
			xml += '\t\t<rows>' + os.linesep

			# write rows to file.
			while selRows > 0 and len(self._rows) > 0:
				selRows -= 1
				row = self._rows.pop(0)
				xml += row.toXml()
			xml += '\t\t</rows>' + os.linesep
			xml += '\t</table>' + os.linesep

			return (rows, xml)
		else:
			return (0, '')

	def truncate(self, cfg, cx=None):
		"""
		Truncates the table (remove all rows).
		Might be necessary in order to do a proper data migration (when you try several times...).
		"""
		log = logging.getLogger('MD-Sync')
		# IBM i Access ODBC driver - at least on linux - does not allow the command "truncate table". 
		# In this circumstances, use a weird DELETE.
		if cfg._dstEngine == 'DB2':
			sql = 'DELETE FROM {:s} WHERE 1=1'.format(self.libtab(cfg._dstEngine),)
		else:
			sql = 'TRUNCATE TABLE {:s}'.format(self.libtab(cfg._dstEngine),)
		if cx:
			log.debug('[{:s}] Truncating table.'.format(self.libtab(cfg._dstEngine),))
			try:
				cx.execute(sql)
			except Exception as e:
				log.error('[{:s}] Truncating table failed, error: {:s}, SQL: {:s}.'.format(
						self.libtab(cfg._dstEngine),
						str(e),
						sql
					)
				)
			else:
				log.info('[{:s}] Table truncated.'.format(self.libtab(cfg._dstEngine),))
			sys.stdout.flush()

		return sql

	def executeInsertSql(self, cfg, cx, sql, noColInsert, insertColList, sqlValues):
		try:
			# if you ever want to read here the real executed SQL: no chance.
			# cf.: https://code.google.com/archive/p/pyodbc/issues/163
			cx.execute(sql, sqlValues)
		except pyodbc.IntegrityError as e:
			if noColInsert == 1:
				# delete the row!
				if self._deleteRow(self.libtab(cfg._dstEngine), insertColList, sqlValues, cx):
					# try to execute again
					try:
						cx.execute(sql, sqlValues)
					except Exception:
						raise e
				else:
					raise e
			else:
				raise e

	def toSql(self, cfg, cx, dryMode=False, shorten=False, noColInsert=1):
		"""
		Converts the table into INSERT statements and also directly executes it!
		Be careful: increasing of noColInsert might produced truncated SQL statements.
		"""
		selectColList = self.colnames(cfg, drop=True)
		insertColList = self.newColNames(cfg)

		# ensure, that the given destination cursor is given!
		if not cx:
			raise Exception('Cannot save data to file, if no cursor is given!')

		baseSql = 'INSERT INTO {:s} ({:s}) VALUES '.format(self.libtab(cfg._dstEngine), ', '.join(insertColList))
		sql = baseSql
		sqlValues = ()
		pendingRows = 0
		for r in self._rows:
			rowList, rowValues = r.toSqlList(selectColList, insertColList, self._table, cfg, dryMode, shorten)
			sql += (', ' if pendingRows > 0 else '') + rowList
			sqlValues += rowValues
			pendingRows += 1
			if pendingRows >= noColInsert:
				self.executeInsertSql(cfg, cx, sql, noColInsert, insertColList, sqlValues)
				sql = baseSql
				sqlValues = ()
				pendingRows = 0

		if pendingRows > 0:
			self.executeInsertSql(cfg, cx, sql, noColInsert, insertColList, sqlValues)
			sql = baseSql
			sqlValues = ()
			pendingRows = 0

	def _deleteRow(self, libTab, colList, colValues, cx):
		"""
		Tries to delete a row from the given library/table. As we do not know the primary keys, we 
		check with all columns!
		"""
		sql = 'DELETE FROM {:s} WHERE'.format(libTab)
		i = 0
		for col in colList:
			if i > 0:
				sql += ' AND'
			sql += ' {:s} = ?'.format(col)
			i+= 1

		try:
			cx.execute(sql, colValues)
		except:
			return False
		else:
			return True

	@classmethod
	def parseXml(cls, parent, cfg):
		lib = parent.getAttribute('library')
		name = parent.getAttribute('name')
		log = logging.getLogger('MD-Sync')
		self = cls(lib, name)
		log.info('[{:s}] Parsing table information'.format(self.libtab(cfg._srcEngine)))
		# for faster parsing, make an index
		colidx = {}

		# get columns
		for child in parent.childNodes:
			if child.nodeType == 1 and child.nodeName == 'columns':
				for colXml in child.childNodes:
					if colXml.nodeType == 1:
						col = Column.parseXml(colXml)
						if col is not None:
							self._columns.append(col)
							colidx[col._name] = col

		# get col data
		for child in parent.childNodes:
			if child.nodeType == 1 and child.nodeName == 'rows':
				for rowXml in child.childNodes: # <row>
					if rowXml.nodeType == 1:
						r = Row.parseXml(colidx, rowXml)
						self._rows.append(r)

		return self

class SyncDataWorker(Thread):
	"""Process request of master to copy data from A to B

	Whenever a request is reached through given *queue*, a client socket and a 
	destination ODBC connection is opened. If this is successful, the regular
	procedure to fetch up to n records, transformed, mapped, dropped and
	finally saved through the completionHook.

	After a table is processed, the *par* is informed via two hooks:
	 - failedCopy in case there was an error and
	 - completedCopy in case it was successful.
	"""

	def __init__(self, jobName, par, lck, queue, cfg, completionHook, 
			totalNumber, dryMode=False, shorten=False, resume=False,
			rowsFetch=100):
		super().__init__()
		self.name = jobName
		self._par = par
		self._lck = lck
		self._q = queue
		self._cfg = cfg
		self._ch = completionHook
		self.dryMode = dryMode
		self.shorten = shorten
		self.totalNumber = totalNumber
		self.resume = resume
		self.rowsFetch = rowsFetch

	def run(self):
		working = True
		srcSock = self._par.getSourceSocket()
		dstSock = self._par.getDestinationSocket()
		log = logging.getLogger('MD-Sync')

		while working:
			try:
				ex = self._q.get(True, 1)
			except queue.Empty:
				working = False
				log.info('No open queue entries anymore. Terminating worker.')
				break
			else:
				self._q.task_done()
				table: Table = None
				with self._lck:
					table = self._par._srcTables[ex]

				tableName = table.libtab(self._cfg._srcEngine)
				log.info('[{:s}] Fetching {:d}/{:d}'.format(tableName, ex + 1, self.totalNumber))
				try:
					table.fetchData(
						sock=srcSock, cfg=self._cfg, dstSock=dstSock, dryMode=self.dryMode, 
						shorten=self.shorten, resume=self.resume, rowsFetch=self.rowsFetch,
						singleExportFile=False, saveHook=self._ch
					)
				except pyodbc.Error as e:
					log.error('[{:s}] Fetching data failed, error: {:s}'.format(tableName, str(e)))
					# inform about complete
					with self._lck:
						self._par.failedCopy(ex)
				else:
					log.info('[{:s}] Fetching data succeeded'.format(tableName))
					# inform about complete
					with self._lck:
						self._par.completedCopy(ex)
				finally:
					# free data up
					table.clear()

		self._par.disconnectSource(srcSock)

class SyncData(object):

	def __init__(self, cfg, output=None, noCombinedInserts=1):
		self._output = output
		self._cfg = cfg
		self._log = None
		self._srcTables = []
		self._dstTables = []
		self._srcSock = None
		self._dstSock = None
		self._srcSockets = []
		self._dstSockets = []
		self._noCombinedInserts = noCombinedInserts
		self._completed = 0
		self._failed = 0

	def configureLogger(self, fpath=None):
		if fpath is None:
			# check + create folders
			os.makedirs('logs', exist_ok=True)
			# generate own file name!
			fpath = '{:s}_{:s}.log'.format(datetime.datetime.now().strftime('%Y-%m-%dT%H%M%S'), 'MD-Sync')
			fpath = os.path.join('logs', fpath)
		self._log = logging.getLogger('MD-Sync')
		logfmt = '%(asctime)-15s %(levelname)s %(module)s.%(funcName)s[%(threadName)s]:%(lineno)d: %(message)s'
		formatter = logging.Formatter(logfmt, datefmt='%b %d %H:%M:%S')
		self._log.setLevel(logging.DEBUG)
		# add new handler
		fh = logging.FileHandler(fpath)
		fh.setFormatter(formatter)
		fh.setLevel(logging.DEBUG)
		self._log.addHandler(fh)

	def prepare(self):
		try:
			os.makedirs(self._output)
		except:
			pass

	def connectOdbcSocket(self, dsn, engine, autocommit=True):
		sock = None
		try:
			sock = pyodbc.connect(dsn, autocommit=autocommit)
		except Exception as e:
			self._log.critical('Could not connect to source: {:s}.\n'.format(str(e)))
			raise
		else:
			if engine == 'MSSQL' and sys.version_info.major < 3:
				sock.setencoding(str, 'latin1')
				sock.setencoding(unicode, 'latin1')
				sock.setdecoding(pyodbc.SQL_CHAR, 'latin1')
				sock.setdecoding(pyodbc.SQL_WCHAR, 'cp1252')

		return sock

	def getSourceSocket(self):
		sock = self.connectOdbcSocket(self._cfg._srcDsn, self._cfg._srcEngine)
		self._srcSockets.append(sock)
		return sock

	def connectSource(self):
		self._srcSock = self.getSourceSocket()

	def disconnectSource(self, sock=None):
		if not sock:
			sock = self._srcSock

		try:
			sock.close()
		except:
			pass

		if sock in self._srcSockets:
			self._srcSockets.remove(sock)

		if sock == self._srcSock:
			self._srcSock = None

	def disconnectSourceSockets(self):
		for sock in self._srcSockets:
			self.disconnectSource(sock)

	def getDestinationSocket(self):
		# check if we use transaction handling.
		transactionUsed = self._cfg._dstTransaction and self._cfg._dstTransaction.lower() in ['true', 'yes', '1', 'y']
		if transactionUsed is None:
			# option not explicitely defined in the configuration file.
			# For non DB2, we try to use the transaction handling.
			# For DB2 the user has explicitely to define it.
			transactionUsed = False if self._cfg._dstEngine == 'DB2' else True
		transactionUsed = not transactionUsed

		sock = self.connectOdbcSocket(self._cfg._dstDsn, self._cfg._dstEngine, transactionUsed)
		self._dstSockets.append(sock)
		return sock

	def connectDestination(self):
		self._dstSock = self.getDestinationSocket()

	def disconnectDestination(self, sock=None):
		if not sock:
			sock = self._dstSock

		try:
			sock.close()
		except:
			pass

		if sock in self._dstSockets:
			self._dstSockets.remove(sock)

		if sock == self._dstSock:
			self._dstSock = None

	def disconnectDestinationSockets(self):
		for sock in self._dstSockets:
			self.disconnectDestination(sock)
			
	def rollback(self):
		if self._dstSockets:
			for sock in self._dstSockets:
				try:
					sock.rollback()
				except Exception as e:
					self._log.error('Could not rollback the transaction: {:s}'.format(str(e)))
				else:
					self._log.info('Rolled back all transactions successfully.')
		else:
			self._log.error('No rollback done as no connection set anymore.')
			
	def commit(self):
		if self._dstSockets:
			for sock in self._dstSockets:
				try:
					self._dstSock.commit()
				except Exception as e:
					self._log.error('Could not commit the transaction: {:s}'.format(str(e)))
				else:
					self._log.info('Commit all transactions successfully.')
		else:
			self._log.error('No commit done as no connection set anymore.')

	def prepareTableData(self, direction='source'):
		"""
		Section prepare the table data to fetch all meta informations
		of a table. 
		This function is used for EXPORT/IMPORT/COPY
		"""
		noTables = 0
		for lib, tablelist in self._cfg._tables.items():
			noTables += len(tablelist)

		# some prework depending on source/destination
		if direction == 'source':
			libEngine = self._cfg._srcEngine
			libSock = self._srcSock
		else:
			libEngine = self._cfg._dstEngine
			libSock = self._dstSock

		self._log.debug('Fetching meta data ({:d} tables)'.format(noTables))
		i = 0
		for lib, tablelist in self._cfg._tables.items():
			for tabentry in tablelist:
				# depending on import / export, we need to get the 
				# correct full library name (considering scheme, abbreviations).
				if direction == 'source':
					library = self._cfg.getFullSourceLib(lib)
				else:
					# we might need to override depending on rename rules.
					if tabentry in self._cfg._tableRename.keys():
						lib, tabentry = self._cfg._tableRename[tabentry].split('/')
					library = self._cfg.getFullDestinationLib(lib)
				i += 1
				tab = Table(library, tabentry)
				self._log.info('[{:s}] Fetching meta information {:d}/{:d}'.format(tab.libtab(libEngine), i, noTables))
				tab.fetchColumns(libSock, libEngine)
				if direction == 'source':
					if tabentry in self._cfg._rules.keys():
						tab.mergeConditions(self._cfg._rules[tabentry])
					self._srcTables.append(tab)
				else:
					self._dstTables.append(tab)
				self.taskDone(i - 1)

	def prepareTableDataSource(self):
		"""
		Section prepare the table data to fetch all meta informations
		of a table. 
		This function is used for EXPORT
		"""
		self.prepareTableData('source')

	def prepareTableDataDestination(self):
		"""
		Section prepare the table data to fetch all meta informations
		of a table. 
		This function is used for IMPORT
		"""
		self.prepareTableData('destination')

	def fetchData(self, singleExportFile=True, noRowsPerFile=1000):
		self._log.info('Fetch table data ({:d} tables)'.format(len(self._srcTables)))
		i = 0
		for table in self._srcTables:
			i += 1
			self._log.info('[{:s}] Fetching data {:d}/{:d}'.format(table.libtab(self._cfg._srcEngine), i, len(self._srcTables)))
			totalRows = table.fetchData(
				sock=self._srcSock, cfg=self._cfg, singleExportFile=singleExportFile, 
				noRowsPerFile=noRowsPerFile, saveHook=self.saveTable
			)
			self.completedCopy(i - 1)
			# free data up
			table.clear()

	def saveTable(self, tab, dstSock=None, singleExportFile=True, noRowsPerFile=None, totRows=0, dryMode=False, shorten=False):
		if singleExportFile:
			self._log.info('[{:s}] Saving gzip-compressed in {:s}'.format(tab.libtab(self._cfg._srcEngine), self._output))
			with gzip.open(self._output, 'ab') as f:
				f.write(self.toXml().encode('utf-8'))
		else:
			# file name
			saveFileNameTpl = os.path.join(self._output, 'export_{:s}_{:s}_{:d}.xml.gz')
			while True:
				rows, tabXml = tab.popXml(noRowsPerFile)
				if rows > 0:
					saveFileName = saveFileNameTpl.format(tab._lib, tab._table, totRows)
					self._log.info('[{:s}] Saving gzip-compressed table {:s}'.format(tab.libtab(self._cfg._srcEngine), saveFileName))
					with gzip.open(saveFileName, 'wb') as f:
						xml  = '<?xml version="1.0" encoding="UTF-8"?>' + os.linesep
						f.write(xml.encode('utf-8'))
						xml = '<EPSYNC>' + os.linesep
						f.write(xml.encode('utf-8'))
						f.write(tabXml.encode('utf-8'))
						xml = '</EPSYNC>' + os.linesep
						f.write(xml.encode('utf-8'))
					totRows += rows
				else:
					break

		return totRows

	def toXml(self):
		xml  = '<?xml version="1.0" encoding="UTF-8"?>' + os.linesep
		xml += '<EPSYNC>' + os.linesep
		for table in self._srcTables:
			xml += table.toXml()
		xml += '</EPSYNC>' + os.linesep

		return xml

	def parseXml(self, xml):
		self._log.info('Parse XML file: {:s}'.format(xml))
		with gzip.open(xml, 'rb') as f:
			data = parseString(f.read())
			root = data.firstChild #EPSYNC
			for child in root.childNodes:
				if child.nodeType == 1:
					if child.nodeName == 'table':
						# parse table
						tab = Table.parseXml(child, self._cfg)
						if tab is not None:
							self._srcTables.append(tab)

	def clearImportData(self):
		"""
		For performance reason, we clear the corresponding srcTables in import process!
		"""
		del(self._srcTables)
		self._srcTables = []

	def _getDestinationTable(self, tabName):
		for tab in self._dstTables:
			if tab._table.lower() == tabName.lower():
				return tab

		return None

	def _prepareImport(self):
		"""
		Prepares the import of data by doing following steps:
		 1. Connect to the corresponding destination
		 2. Fetch all meta information
		"""
		if self._dstSock is None:
			self.connectDestination()

		# prepare information about destination
		self.prepareTableDataDestination()

	def importDataSetup(self, numFiles):
		self.progress = tqdm.tqdm(desc='Prepare import', total=numFiles)

	def importData(self, dryMode=False, shorten=False):
		numTables = sum([len(tableList) for tableList in self._cfg._tables.values()])
		if self._dstSock is None:
			self.progress.total += numTables + numTables*3
			self._prepareImport()
		else:
			# metadata is already fetched
			self.progress.total += numTables*3

		self._log.debug('Importing table data')
		self.progress.set_description(desc='Import data')
		for tab in self._srcTables:
			incidentOccured = False
			self._log.info('[{:s}] Start import process'.format(tab.libtab(self._cfg._dstEngine),))
			tab.updateDestination(self._cfg._dstKku, self._cfg._dstVersion)
			self._log.info('[{:s}] Destination updated for table'.format(tab.libtab(self._cfg._dstEngine),))
			# check if there is the same table in new system.
			prevName = tab.libtab(self._cfg._dstEngine)
			tab._origLib = tab._lib
			tab._origTable = tab._table.upper()
			if tab._table.upper() in self._cfg._tableRename.keys():
				dstTableLib, dstTableName = self._cfg._tableRename[tab._table.upper()].split('/')
				tab.renameTable(dstTableLib, dstTableName.upper())
				self._log.info('[{:s}] renamed to {:s}'.format(prevName, tab.libtab(self._cfg._dstEngine)))
			sys.stdout.flush()
			dstTable = self._getDestinationTable(tab._table)
			if dstTable:
				tab.applyRules(self._cfg, dstTable)
				# everything prepared. Now check if we have to clean up this table prior import of the data
				cx = self._dstSock.cursor()
				try:
					# yes, its correct: also we might have configured to rename the table, we have to lookup the 
					# rules for the table based on old name.
					if tab._origTable in self._cfg._truncations and self._cfg._truncations[tab._origTable]:
						tab.truncate(self._cfg, cx)
						try:
							self._cfg._truncations[tab._origTable] = False
						except:
							pass
				except Exception as e:
					self._log.error('[{:s}] Import failed.'.format(prevName))
					self._log.error('[{:s}] Error occured during truncation: {:s}'.format(prevName, str(e)))
					incidentOccured = True
				else:
					try:
						tab.toSql(self._cfg, cx, dryMode, shorten)
					except Exception as e:
						self._log.error('[{:s}] Import failed.'.format(prevName))
						self._log.error('[{:s}] Error occured during execution: {:s}'.format(prevName, str(e)))
						incidentOccured = True
					else:
						self._log.info('[{:s}] Data imported successfully to {:s}'.format(prevName, tab.libtab(self._cfg._dstEngine)))
				finally:
					cx.close()
			else:
				# how can this happen?
				# Based on configuration we read all meta information of the affected tables on the destination
				# system. If the user manipulates the files now and changes the configuration, it may happen, that
				# the XML content does not match the configuration anymore, but the filename itself matches to the
				# configuration file whereas the file is not discarded. But we do not find any table meta information
				# as we never read data into the system based on the name.
				self._log.error('[{:s}] Import failed.'.format(prevName))
				self._log.error('[{:s}] Could not retrieve destination table information. Revise configuration.'.format(prevName,))
				incidentOccured = True
			self.progress.update(3)
			# stop immediately if an incident occured (in non-dry-only-mode)
			if incidentOccured and not dryMode:
				break

		return incidentOccured

	def exportData(self, singleExportFile=True, noRowsPerFile=1000):
		if not singleExportFile:
			self.prepare()

		if self._srcSock is None:
			self.connectSource()

		# start progress
		numTables = sum([len(tableList) for tableList in self._cfg._tables.values()])
		totalProgressNumber = numTables + numTables*3
		self.progress = tqdm.tqdm(desc='Export metadata', total=totalProgressNumber)
		# prepare the table information.
		self.prepareTableDataSource()

		# fetch data
		self.progress.set_description('Export table data')
		self.fetchData(singleExportFile, noRowsPerFile)

	def copyData(self, dryMode=False, shorten=False, numberJobs=1, resume=False, rowsFetch=100):
		"""
		This method will copy data based on the configuration from source directly to their destination.
		"""
		numTables = sum([len(tableList) for tableList in self._cfg._tables.values()])
		# calculate total progress
		#   open ODBC connections: 2
		# + source metadata: noTables
		# + destination metaData: noTables
		# + copy table: noTables * 3
		# + comit/rollback: 1
		totalProgressNumber = 2 + numTables*2 + numTables*3 + 1
		self.progress = tqdm.tqdm(desc='Prepare metadata source', total=totalProgressNumber)

		# connect source
		self.connectSource()
		self.progress.update()
		# fetch meta information of all requested source files.
		self.prepareTableDataSource()

		# connect to the destination server
		self.progress.set_description('Prepare metadata dest')
		self.connectDestination()
		self.progress.update()
		# fetch meta information of all requested destination files.
		self.prepareTableDataDestination()

		# prepare queue
		totalNumberTables = len(self._srcTables)
		workQueue = JoinableQueue()
		tableLock = Lock()
		processes = []
		self._completed = self._failed = 0
		startDt = datetime.datetime.now()
		self._log.info('Started copy process at {:s} for {:d} tables'.format(startDt.strftime('%d.%m.%Y %H:%M:%S'), totalNumberTables))
		self.progress.set_description('Copy data')
		i = 0
		try:
			for i in range(0, totalNumberTables):
				workQueue.put(i)

			# create processes
			for i in range(0, numberJobs):
				jobName = 'mdsync-thread-#{:d}'.format(i)
				p = SyncDataWorker(
					jobName, self, tableLock, workQueue, self._cfg, self.writeDestinationRecord, totalNumberTables, 
					dryMode, shorten, resume, rowsFetch
				)
				p.start()
				self._log.debug('Worker process {:s} started!'.format(jobName))
				processes.append(p)

			# Keep going until all tables are processed.
			while (self._completed + self._failed) < totalNumberTables:
				time.sleep(1)
				self.progress.refresh()

			self._log.info('All processes for copying data finished.')

			# don't expect any work anymore.
			workQueue.close()

			# Indicate error and rollback if any kind of issue occured.
			if self._failed > 0:
				raise Exception('See previous message, direct copy failed.')
		except Exception as e:
			self._log.error('During copy of data, an error occured: {:s}'.format(str(e)))
			self._log.debug('Traceback for prev. error: {:s}'.format(traceback.format_exc()))
			self.rollback()
		else:
			# everything worked fine till now.
			# commit?
			if dryMode:
				self.rollback()
			else:
				self.commit()
		finally:
			self.disconnectSourceSockets()
			self.disconnectDestinationSockets()
			self.progress.update()

		endDt = datetime.datetime.now()
		self._log.info('Ended at: {:s}'.format(endDt.strftime('%d.%m.%Y %H:%M:%S')))
		totalDt = endDt - startDt
		self._log.info('Took in total {:d}s'.format(totalDt.seconds))

	def taskDone(self, idx=None, pt=1):
		try:
			if self.progress:
				self.progress.update(pt)
		except AttributeError:
			pass

	def completedCopy(self, idx):
		self._completed += 1
		self.taskDone(idx, 3)

	def failedCopy(self, idx):
		self._failed += 1
		self.taskDone(idx, 3)

	def writeDestinationRecord(self, tab, dstSock, singleExportFile, noRowsPerFile, totalRows, dryMode=False, shorten=False):
		# clone table to keep everything correct.
		tabWorking = copy.deepcopy(tab)
		tabWorking.updateDestination(self._cfg._dstKku, self._cfg._dstVersion)
		tabWorking._origLib = tabWorking._lib
		tabWorking._origTable = tabWorking._table.upper()
		prevName = tabWorking.libtab(self._cfg._dstEngine)
		self._log.debug('[{:s}] Writing records'.format(prevName,))
		# user can also rename the table during import.
		if tab._table.upper() in self._cfg._tableRename.keys():
			dstTableLib, dstTableName = self._cfg._tableRename[tab._table.upper()].split('/')
			tabWorking.renameTable(dstTableLib, dstTableName.upper())
			self._log.debug('[{:s}] renamed during writing to {:s}'.format(prevName, tabWorking.libtab(self._cfg._dstEngine)))
		sys.stdout.flush()
		# check if there is the same table in new system.
		dstTable = self._getDestinationTable(tabWorking._table)
		if dstTable:
			tabWorking.applyRules(self._cfg, dstTable)
			# everything prepared. Now check if we have to clean up this table prior import of the data
			cx = self._dstSock.cursor() if not dstSock else dstSock.cursor()
			# on first record we may need to truncate the table!
			try:
				# yes, its correct: also we might have configured to rename the table, we have to lookup the 
				# rules for the table based on old name.
				if tab._table.upper() in self._cfg._truncations and self._cfg._truncations[tab._table]:
					tabWorking.truncate(self._cfg, cx)
					try:
						self._cfg._truncations[tab._table.upper()] = False
					except Exception as e:
						self._log.error(
							'[{:s}] Issue in writeDestinationRecord: could not remove table from truncation: {:s}'.format(
								prevName, str(e)
							)
						)
			except Exception as e:
				self._log.error('[{:s}] failed during copy due to {:s}'.format(prevName, str(e)))
				raise e
			else:
				try:
					tabWorking.toSql(self._cfg, cx, dryMode=dryMode, shorten=shorten, noColInsert=self._noCombinedInserts)
				except Exception as e:
					self._log.error('[{:s}] failed during copy due to {:s}'.format(prevName, str(e)))
					raise e
				else:
					self._log.info('[{:s}] Copy was successful'.format(prevName,))
			finally:
				cx.close()
		else:
			# how can this happen?
			# Based on configuration we read all meta information of the affected tables on the destination
			# system. If the user manipulates the files now and changes the configuration, it may happen, that
			# the XML content does not match the configuration anymore, but the filename itself matches to the
			# configuration file whereas the file is not discarded. But we do not find any table meta information
			# as we never read data into the system based on the name.
			self._log.error('[{:s}] failed as destination table information could not be retrieved.'.format(prevName,))
		# all data insert. Clear data.
		tab.clear()

		return totalRows


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Sync of data through multiple systems.')
	subparsers = parser.add_subparsers(help='sub-command help', required=True, dest='command')

	# Export of data
	expParser = subparsers.add_parser('export', help='Export options', description='Use to export and store the configured data in corresponding data directory')
	expParser.add_argument('--config', '-c', type=str, help='Load a specific configurations.', required=True)
	expParser.add_argument(
		'--rows', dest='maxrows', type=int, help='Defines max. number of rows in each export file (default: 1000)',
		 required=False, default=1000
	)
	expParser.add_argument('dataDirectory', type=str, help='Store/Load data from given data directory.')

	# import of data
	impParser = subparsers.add_parser('import', help='Import options', description='Use to import of provided data in corresponding data directory.')
	impParser.add_argument('--config', '-c', type=str, help='Load a specific configurations.', required=True)
	impParser.add_argument('--dry-run', dest='dryRun', action='store_const', help='Do a dry run before real import', required=False, const=True, default=False)
	impParser.add_argument(
		'--shorten', dest='shorten', action='store_const', help='Auto short alphanumerics variables which do not fit to destination variable', 
		required=False, const=True, default=False
	)
	impParser.add_argument('dataDirectory', type=str, help='Store/Load data from given data directory.')

	# direct copy of data
	cpyParser = subparsers.add_parser('copy', help='Copy options')
	cpyParser.add_argument('--config', '-c', type=str, help='Load a specific configurations.', required=True)
	cpyParser.add_argument('--dry-run', dest='dryRun', action='store_const', help='Do a dry run before real import', required=False, const=True, default=False)
	cpyParser.add_argument(
		'--shorten', dest='shorten', action='store_const', help='Auto short alphanumerics variables which do not fit to destination variable', 
		required=False, const=True, default=False
	)
	cpyParser.add_argument('--jobs', '-j', type=int, dest='jobs', help='Number of jobs to connect parallel (def.: 1)', required=False, default=1)
	cpyParser.add_argument('--rows-fetch', type=int, dest='rowsFetch', help='Number of rows downloaded and kept in memory (def.: 100)', required=False, default=100)
	cpyParser.add_argument('--inserts', type=int, dest='noCombinedInserts', help='Number of combined insert statement on output (def.: 1)', required=False, default=1)
	cpyParser.add_argument(
		'--resume', '-r', dest='resume', action='store_const', 
		help='Try to resume previous run (e.g. in case of connection abort). Works only in case file was truncated before.', 
		required=False, const=True, default=False
	)

	# parse data
	args = parser.parse_args()

	# load config!
	sc = SyncConfig(args.config)
	sc.parseConfig()
	
	# execute the right action depending on the given command!
	if args.command == 'export':
		sc.compileSourceDsn()
		sd = SyncData(sc, args.dataDirectory)
		sd.configureLogger()
		try:
			sd.exportData(singleExportFile=False, noRowsPerFile=args.maxrows)
		except Exception as e:
			sd._log.error('Exception during exporting data: {:s}, stack: {:s}'.format(str(e), traceback.format_exc()))
			raise e
		finally:
			sd.disconnectSourceSockets()
	elif args.command == 'import':
		sc.compileDestinationDsn()
		sd = SyncData(sc, args.dataDirectory)
		sd.configureLogger()
		sd._log.info('Execution mode: {:s}'.format('Dry run' if args.dryRun else 'Production',))
		incidentOccured = False
		try:
			fileList = glob.glob(os.path.join(args.dataDirectory, '*.xml.gz'))
			sd.importDataSetup(numFiles=len(fileList))
			for fname in fileList:
				if sc.isPartOfConfig(fname):
					sd.parseXml(fname)
					incidentOccured = sd.importData(args.dryRun, args.shorten)
					sd.clearImportData()
					# in case of non-dry-run mode, we need to stop in case of incidents.
					if not args.dryRun and incidentOccured:
						raise Exception('Incident occured during import. Cancel process.')
				else:
					sd._log.warning('Skip file {:s} as not mentioned in config file.'.format(os.path.basename(fname)))
		except MemoryError as e:
			sd._log.critical('Got a memory error: ' + str(e))
			#sd.rollback()
			sd.commit()
		except Exception as e:
			sd._log.error(str(e))
			sd.rollback()
		else:
			if args.dryRun:
				sd.rollback()
			else:
				sd.commit()
		finally:
			# disconnect from destination.
			sd.disconnectDestinationSockets()
	elif args.command == 'copy':
		# create / build up the connection string for source **and** destination.
		sc.compileDsn()
		sd = SyncData(sc, noCombinedInserts=args.noCombinedInserts)
		sd.configureLogger()
		sd._log.info('Execution mode: {:s}'.format('Dry run' if args.dryRun else 'Production',))
		sd.copyData(args.dryRun, args.shorten, args.jobs, args.resume, args.rowsFetch)
	else:
		parser.print_help()
		sys.exit(1)
