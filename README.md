# MD-Sync
> Data migration in two steps without taking care of plattform and database engine

You know this? You have to do some data migration with similar, but different table design as your software is evolving day per day? And then suddenly the plattform changes. It makes the data migration hard. Especially if the development happens on a different plattform or with different database engine than the production server is running. In worst case even the encoding changed.

In order to solve this, this tool was designed. It makes the data migration under specific circumstances easier. It works with table schemes, which must be in a defined format.

## Version
The version you have in your hands is the latest stable release with version 1.3.0.

## Installing / Getting started

The software is a one-source software and just needs to have python installed and a small package. To install the requirements (beside of python), please execute following statement:

```shell
python -m pip install -r requirements.txt
```

This statement requires, that the `pip` module is installed. It will install all dependencies listed in the file.

In order to use this MD-Sync tool, you should have installed the correct ODBC driver for your plattform and database.

### Initial Configuration

You must prepare a configuration file per project. You can find examples in `example-config.xml` and `example-config-kku.xml`. This configuration is loaded on every operation (e.g. import or export). You do not need to have the same configuration file on source machine and on destination machine (e.g. to ensure, that no password or infrastructure details are exploited on the remote machine).

## Developing

Development is easy. Clone it, modify it, execute it!

```shell
git clone https://bitbucket.org/euplus/md-sync.git
cd md-sync/
```

This will first clone this project and will change to the working directory.

You can start modifying the project.

## Features

What's all the bells and whistles this project can perform?

* Export data from a database for backup or copying reason
* "Manipulate" the data during migration to another server by
 - dropping columns
 - transforming data based on format rules to a new destination column
 - renaming off columns
 - replace column content based on conditions
 - destination table can have a different name
* Pre-hook: truncate a table prior importing the data
* Automatic update of existing records by deleting them (**slow [insert -> delete -> insert]!**)
* Import of the data to another database (different plattform, different database engine)
* Direct copy of data from table A to table B
* Validates whether alphanumeric data still fits into new table

## Configuration

This software is using a XML file structure in order to configure.
The configuration file consists of different sections.

### Connection
A connection has a `source` and a `destination`. Both have the same configuration options:

#### engine
Contains the database engine to use. E.g. `DB2`, `MSSQL`, `ORACLE`, `PSQL`, `MARIADB`.

PostgreSQL (`PSQL`) is tested in version 9.6 (as destination, not as source).
Note, while moving data from one system to another: PostgreSQL converts all column to lower case, as long as column name is not given in quote chars (not supported here). Other engines like e.g. MariaDB are case sensitive and if the columns name does not match for import / sync, data cannot be synced yet.

#### transaction
Since version `1.0` of this tool, transaction functionality is used in order to easy rollback during data migration process. This works as long as the database engine supports this feature. 
Migrating data to a DB2 engine works only with journal enabled and active. If it was not enabled, the system was not able to migrate the data. In order to solve this, the transaction functionality can be enabled (`true`) or disabled (`false`). 

If this field is not specified, the default value will be used. This differs depending on the database engine. DB2 is disabled by default. On non DB2 engines, the option is enabled by default.

#### user
User name for the database connection

#### pwd
Optional! You can specify the password directly in the configuration file. The password must be base64 encoded so that the reader does not directly read and remembers the password. **It is not recommended to store the password directly in the configuration file**. If the password is not given, it is asked on the command line.

#### dsn
Database connection string. Depends on the driver. See the configuration examples of the `pyodbc` project.

Parameters like {user} and {pwd} are replaced by the corresponding configuration options.

#### kku
Abbreviation of the customer. Used to build some schemes.

#### version
Major version number of the customer project. Used to build some schemes.

#### release
Release version of the used software customer project.

### Tables

Contains more or less all tables which should be ex-/imported. Several options are available for a table.

#### Truncate
Applies to the **import** process.

Example:
```xml
<tables>
	<CAT>
		<TABLE1 truncate="true" />
	</CAT>
</tables>
```

With this attribute set to `true`, the table is truncated prior the data is imported. This option is helpful if you try several times to synchronize data between several machines.

#### Destination
Applies to the **import** process.

Example:
```xml
<tables>
	<CAT>
		<TABLE1 destination="CT2/TABLE1A" />
	</CAT>
</tables>
```

With this attribute set to `scheme/table`, the table is imported under a different name than exported. In above given example, the file TABLE1 is exported from CAT library and imported into TABLE1A of CT2 library.

#### Condition
Applies to the **export** process.

It is possible to limit the data from the table based on some conditions which are given in the `condition` tags. 

Example:
```xml
<TABLE2>
	<condition>FLD01 &lt;&gt; ' ' AND FLD02 &lt;&gt; '000'</condition>
</TABLE2>
```

This snippet will ensure, that only those data are exported, which match that condition.
The conditions do **not** apply to the import process.

### Mapping

Per table it is possible to define several different operations (merge, replace, drop, rename)

#### merge
Example:
```xml
<TABLE1>
	<merge>
		<TZDTLI format="{TZLIJH:0>2d}{TZLIJA:0>2d}{TZLIMO:0>2d}{TZLITA:0>2d}">
			<TZLIJH />
			<TZLIJA />
			<TZLIMO />
			<TZLITA />
		</TZDTLI>
	</merge>
</TABLE1>
```
This will fill the `TZDTLI` column with the data from `TZLIJH`, `TZLIJA`, `TZLIMO` and `TZLITA` based on the formatting given in attribute `format`. Python syntax!

#### replace
Example:
```xml
<TABLE1>
	<replace>
		<FLD01 when="100">356</TZFIRM>
	</replace>
</TABLE1>
```

This snippet will replace the content of the column `FLD01` with `356` whenver the value is `100`.

#### drop
Example:
```xml
<TABLE1>
	<drop>
		<TZLIJH />
		<TZLIJA />
		<TZLIMO />
		<TZLITA />
	</drop>
</TABLE1>
```

If columns were removed in new version of your software, it is possible to drop those columns. 

#### rename
Example:
```xml
<TABLE3>
	<rename>
		<TAB_FLD01>TAB3_FLD01</TAB_FLD01>
	</rename>
</TABLE3>
```

During data migration, the column `TAB_FLD01` is renamed to `TAB3_FLD01`. The column `TAB3_FLD01` will contain the data of the previous existing column `TAB_FLD01`.

## Executing

### Exporting of data
For exporting, you can call the project:
```bash
python3 copy-master-data.py export -c example-config-lus.xml data/
```

This will call the script `copy-master-data.py` considering the configuration file `example-config-lus.xml` and will export the data into the folder `data/`. This folder will be created if it does not exist.

There is a parameter `--rows N` available, whereas `N` specifies the number of rows which should be put in one export file. By default its 1000 rows.

### Importing of data
For importing, you can call the project:
```bash
python3 copy-master-data.py import -c example-config-lus.xml data/
```

This will call the script `copy-master-data.py` considering the configuration file `example-config-lus.xml` and will import the data from folder `data/`.

### Direct copy of data
If you want to copy the data without flatfiles from system A to system B, just call the project like:
```bash
python3 copy-master-data.py copy -c example-config-lus.xml data/
```

This will call the script `copy-master-data.py` considering the configuration file `example-config-lus.xml` and will copy the data from A to B (directly through ODBC).

#### Special option: number of jobs
For direct copy operation, the option `--jobs` is available. By default, the option is 1. It must be a numeric one. It indicates, with how much jobs the data is copied in parallel (several ODBC connections are opened up).

#### Special option: inserts
For direct copy operation, the option `--inserts` (default: 1) specified, how much rows are inserted within one `INSERT` statement. Be careful in selecting the right value. In certain cases it can lead to too long SQL statements which cannot be executed via ODBC. This is not checked. On the other side, it improves the insert speed.

#### Resume operation
For direct copy operation, it is possible to resume aborted copy operations with options `--resume` - or shortform `-r`. This works only in case there was no data manipulation on destination table in between. It is checked via `COUNT` on destination, how much records were already written and those number of records are skipped. Of course, this function works only in case initial copy process was initiated with `truncate` option was set. For resume, the `truncate` is skipped.

### Dry-Run and auto shorten data
In most cases there is no overview about all changes between old and new tables. Sometimes the same column has different definition in new table and the data does not fit anymore. In order to validate the data, you can run in dry-run mode:
```bash
python3 copy-master-data.py copy -c example-config-lus.xml --dry-run data/
```

And if there are errors shown like `Error message: Error preparing data in table ABC, column XYZ: data "sdfjaslfjasleflasdjfafasf" too long (max: 11, is: 25)`, you can automatically shorten the data with help of `--shorten` parameter:
```bash
python3 copy-master-data.py copy -c example-config-lus.xml --shorten data/
```

### Logging
Meanwhile a logging is implemented. Output file and verbosity level cannot be configured yet. Each operation will create a separate log file within the `logs/` folder. 

### Getting help of commands available

Whenever some information about commands are required, the `-h` parameter is available:
```bash
python3 copy-master-data.py -h
```

And for specific sub commands:
```bash
python3 copy-master-data.py copy -h
```

## Contributing

If you'd like to contribute, please fork the repository and use a feature
branch. Pull requests are warmly welcome. We cannot cover everything from beginning. Your experience and expertise is necessary to make a awesome product out of it!

## Links

- Project homepage: https://md-sync.lschreiner.de
- Repository: https://bitbucket.org/euplus/md-sync/
- **Issue tracker:** https://bitbucket.org/euplus/md-sync/issues
  - In case of sensitive bugs like security vulnerabilities, please contact
    dev@lschreiner.de directly instead of using issue tracker. We value your effort
    to improve the security and privacy of this project!

## Licensing

The code in this project is licensed under MIT license.
